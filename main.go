package main

import (
	_ "fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/template/html"
	"gitlab.com/genesis.elliott/letzee/lib/routes"
	_ "gitlab.com/genesis.elliott/letzee/lib/tools"
)

var (
	lxdInstances []map[string]string
)

func main() {

	/*fmt.Println(">> LXDs that are Running")
	runningLXDs := tools.GetLXDs(tools.RUNNING)
	for _, i := range *runningLXDs {
		fmt.Printf("%q\n", i)
	}

	fmt.Println("\n>> LXDs that are Stopped")
	stoppedLXDs := tools.GetLXDs(tools.STOPPED)
	for _, i := range *stoppedLXDs {
		fmt.Printf("%q\n", i)
	}

	fmt.Println("\n>> ALL LXDs")
	allLXDs := tools.GetLXDs(tools.ALL)
	for _, i := range *allLXDs {
		fmt.Printf("%q\n", i)
	}*/

	engine := html.New("./views", ".html")
	app := fiber.New(fiber.Config{
		Views: engine,
	})

	app.Static("/css", "./css")
	app.Static("/js", "./js")
	app.Static("/templ", "./templ")

	app.Get("/", routes.Root)
	app.Get("/getlxd/:status?", routes.GetLXDs)
	app.Get("/getlxdinfo/:name", routes.GetLXDInfo)

	app.Listen(":3333")

}
