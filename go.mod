module gitlab.com/genesis.elliott/letzee

go 1.15

require (
	github.com/gofiber/fiber/v2 v2.3.0
	github.com/gofiber/template v1.6.6
)
