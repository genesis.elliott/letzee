package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	_ "letzee/libs/lxdcli"
)

func main() {
	r := gin.Default()

	r.Static("/assets", "./assets")

	r.GET("/", root)
	r.Run()
}

func root(c *gin.Context) {
	// check for Accept header
	if v, ok := c.Request.Header["Accept"]; ok {
		fmt.Printf("Accept => %s\n", v)
	}
	c.File("local/index.html")
}
