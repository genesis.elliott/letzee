package routes

import (
	"github.com/gofiber/fiber/v2"
	// debug

	// disabled
	"fmt"
	_ "github.com/gofiber/template/html"
	"gitlab.com/genesis.elliott/letzee/lib/tools"
)

// LXDInfo lxd container info
type LXDInfo struct {
	SysInfo *map[string]string
	NetInfo *[][]string
}

// Root root URL
func Root(c *fiber.Ctx) error {
	return c.SendString("root")
}

func GetLXDInfo(c *fiber.Ctx) error {

	retVal := LXDInfo{}

	c.Set("Content-Type", "text/html")
	name := c.Params("name")

	lxdInfo, lxdNetInfo := tools.GetLXDInfo(name)
	//return c.SendString(fmt.Sprintf("%s => %v", name, lxdInfo))

	retVal.SysInfo = lxdInfo
	retVal.NetInfo = lxdNetInfo

	fmt.Printf("%q\n", retVal.SysInfo)
	fmt.Printf("%q\n", retVal.NetInfo)

	return c.Render("lxdinfo", retVal, "layouts/default")
}

// GetLXDs get a list of LXD instances
func GetLXDs(c *fiber.Ctx) error {
	c.Set("Content-Type", "text/html")

	statusStr := c.Params("status", "all")
	statusInt := tools.ALL

	if statusStr == "running" {
		statusInt = tools.RUNNING
	} else if statusStr == "stopped" {
		statusInt = tools.STOPPED
	}

	lxdList := tools.GetLXDs(statusInt)

	return c.Render("index", *lxdList, "layouts/default")
}
