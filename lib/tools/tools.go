package tools

import (
	"os/exec"
	"regexp"

	// For extracting the stdout and stderr line by line
	"bufio"
	// To store our stdout and stderr for use with bufio
	"bytes"
	_ "fmt"
	"log"
	"strings"
)

const (
	// ALL LXDs
	ALL = iota
	// RUNNING - Running LXDs
	RUNNING
	// STOPPED - Stopped LXDs
	STOPPED
)

// Exec executes external command
func Exec(command string, args ...string) (*bytes.Buffer, *bytes.Buffer) {

	cmdErr := &bytes.Buffer{}
	cmdOut := &bytes.Buffer{}

	cmd := exec.Command(command, args...)
	cmd.Stdout = cmdOut
	cmd.Stderr = cmdErr

	err := cmd.Run()
	if err != nil {
		scanner := bufio.NewScanner(cmdErr)
		for scanner.Scan() {
			log.Println(scanner.Text())
		}
		log.Fatal(err)
	}

	return cmdOut, cmdErr
}

// GetLXDInfo Gets an info of an LXD container
func GetLXDInfo(name string) (*map[string]string, *[][]string) {
	var lxdInfo = make(map[string]string)
	var lxdNetInfo = make([][]string, 0)

	cmdOut, _ := Exec("lxc", "info", name)

	rgxPrimaryInfo := regexp.MustCompile(`^\w+:\s+\w+`)
	//rgxNetInfo := regexp.MustCompile(`^\s+\w+:\s+inet.+`)

	//rgxIP := regexp.MustCompile(`^\s+(\w+):\s+(inet|inet6)\s+(\w+)\s+`)
	rgxIP := regexp.MustCompile(`^\s+(\w+):\s+(inet6|inet)\s+(\S+)\s+(\w+)`)

	//rgxSpaceInFront := regexp.MustCompile(`^\s+`)

	scanner := bufio.NewScanner(cmdOut)

	for scanner.Scan() {
		line := scanner.Text()

		if rgxPrimaryInfo.MatchString(line) {
			lineSplit := regexp.MustCompile(`:\s+`).Split(line, 2)
			lxdInfo[strings.ToLower(lineSplit[0])] = lineSplit[1]
		} else if rgxIP.MatchString(line) {
			m := rgxIP.FindAllStringSubmatch(line, -1)
			g := m[0]
			//fmt.Printf("%q => %d\n", match, len(match[0]))
			lxdNetInfo = append(lxdNetInfo, []string{g[1], g[2], g[3], g[4]})
		}
	}

	return &lxdInfo, &lxdNetInfo
}

// GetLXDs Gets a list of LXDs
func GetLXDs(status int) *[]map[string]string {

	statusStr := "RUNNING|STOPPED"

	if status == RUNNING {
		statusStr = "RUNNING"
	} else if status == STOPPED {
		statusStr = "STOPPED"
	}

	var lxdInstances []map[string]string
	/*
		Pre-defined column shorthand chars:
		4 - IPv4 address
		6 - IPv6 address
		a - Architecture
		b - Storage pool
		c - Creation date
		d - Description
		D - disk usage
		l - Last used date
		m - Memory usage
		n - Name
		N - Number of Processes
		p - PID of the instance's init process
		P - Profiles
		s - State
		S - Number of snapshots
		t - Type (persistent or ephemeral)
		L - Location of the instance (e.g. its cluster member)
		f - Base Image Fingerprint (short)
		F - Base Image Fingerprint (long)
	*/

	// | RUNNING | midlnx01        | 10.180.74.116 (eth0) | x86_64       | default      | 742.24MB     |
	cmdOut, _ := Exec("lxc", "ls", "-c", "sn4abm")

	// Parse the output
	scanner := bufio.NewScanner(cmdOut)
	for scanner.Scan() {

		m, _ := regexp.MatchString(statusStr, scanner.Text())
		if m {

			//val := regexp.MustCompile("\\s*\\|\\s*$").ReplaceAllString(scanner.Text(), "")
			val := regexp.MustCompile(`\s*\|\s*$`).ReplaceAllString(scanner.Text(), "")

			//val = regexp.MustCompile("^\\s*\\|\\s*").ReplaceAllString(val, "")
			val = regexp.MustCompile(`^\s*\|\s*`).ReplaceAllString(val, "")

			//strS := regexp.MustCompile("\\s*\\|\\s*").Split(val, -1)
			strS := regexp.MustCompile(`\s*\|\s*`).Split(val, -1)
			//fmt.Printf("%q\n", strS)

			// sn4abm
			// 0: STATUS, 1: NAME, 2: IPV4, 3: ARCH, 4: STORAGE POOL, 5: MEMORY USAGE
			lxdInstances = append(lxdInstances, map[string]string{
				"status":  strS[0],
				"name":    strS[1],
				"ipv4":    strS[2],
				"arch":    strS[3],
				"storage": strS[4],
				"memory":  strS[5],
			})
		}
	}

	return &lxdInstances
}
