#!/usr/bin/ruby

"""
Name: svn01
Location: none
Remote: unix://
Architecture: x86_64
Created: 2021/03/19 09:04 UTC
Status: Running
Type: container
Profiles: default
Pid: 3495
Ips:
  eth0:	inet	10.180.74.249	veth0e2a948f
  eth0:	inet6	fd42:dd:7a16:93bf:216:3eff:fe2b:2080	veth0e2a948f
  eth0:	inet6	fe80::216:3eff:fe2b:2080	veth0e2a948f
  lo:	inet	127.0.0.1
  lo:	inet6	::1
Resources:
  Processes: 52
  CPU usage:
    CPU usage (in seconds): 12
  Memory usage:
    Memory (current): 346.08MB
    Memory (peak): 374.38MB
  Network usage:
    eth0:
      Bytes received: 180.58kB
      Bytes sent: 23.95kB
      Packets received: 1352
      Packets sent: 160
    lo:
      Bytes received: 1.70kB
      Bytes sent: 1.70kB
      Packets received: 20
      Packets sent: 20
"""

data = {}

File.open('lxdinfo_output.txt') do |file|
  file.each do |line|
    if line =~ /^\S+/ then
      line.chomp!
      key, val = line.split(/:\s+/, 2)
      data[key] = val
    end
  end
end

puts data
